# -*- coding: utf-8 -*-
"""
@author: nilesh

Tasks: Completed


Tasks: Pending
1. Create data pipeline Reuters-21578

Refrences:
Convolutional Neural Networks for Sentence Classification || http://arxiv.org/pdf/1408.5882v2.pdf


Refrences Data:
Large Movie Review Dataset (IMDB Dataset) || aclImdb_v1.tar.gz || http://ai.stanford.edu/~amaas/data/sentiment/
Sentiment polarity datasets || rt-polaritydata.tar.gz || http://www.cs.cornell.edu/people/pabo/movie-review-data/

External Repos:
CNN-for-Sentence-Classification implementation using keras || https://github.com/alexander-rakhlin/CNN-for-Sentence-Classification-in-Keras/blob/master/trainGraph.py

"""
from __future__ import print_function
import sys
import os
import timeit
import copy
import numpy as np
import pandas as pd
from multiprocessing import cpu_count

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.decomposition import TruncatedSVD
from sklearn.pipeline import Pipeline
from sklearn.linear_model import LogisticRegression, SGDClassifier
from sklearn.svm import SVC
from sklearn.naive_bayes import MultinomialNB
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier
from sklearn.ensemble import ExtraTreesClassifier, AdaBoostClassifier

from keras.models import Model
from keras.callbacks import EarlyStopping
from keras import backend as K    
from keras.models import Sequential
from keras.layers.core import Dense, Activation
from keras.wrappers.scikit_learn import KerasClassifier
from keras.layers import Dropout, Lambda, Embedding, LSTM
from keras.layers import Convolution1D


# User Inputs -----------------------------------------------------------------
path_base = os.path.dirname(os.path.dirname(os.path.realpath(__file__))) 
fl_inp_train = '25K_classified.csv'
fl_inp_test = '25K_classified.csv'
# fl_submission_input = 'sample_submission.csv'
fl_sent_polarity_pos = 'rt-polaritydata/rt-polarity.pos'
fl_sent_polarity_neg = 'rt-polaritydata/rt-polarity.neg'
fl_sent_polarity_pre_proc = 'sentiment_polarity_pre_proc.csv'
fl_word_vectors = 'ab_glove.6B.300d.txt'


# Set Base Path----------------------------------------------------------------
os.chdir(path_base)
sys.path.append(path_base)
from q_scripts.a_class_func_dir import DirStructure, Job, Data, Word2VecTransformer
from q_scripts.a_class_func_dir import TextToNumericSequence, PadNumericSequence



# Read Config File: -----------------------------------------------------------
fld = DirStructure('config.ini')


def aa_tfidf_multinon(data):
    job = Job('aa_tfidf_multinon')
    pipeline = Pipeline(steps=[("tfidf", TfidfVectorizer(stop_words = 'english',
                                                         min_df = 5)),
                               ('logistic', LogisticRegression())])
    parameters = dict(tfidf__norm = ['l1', 'l2'],
                      tfidf__ngram_range = [(1, 2)],
                      logistic__C = np.logspace(-2, 2, 5))
    job.run(pipeline, parameters, data)
    return None
    
def ab_tfidf_elasticnet(data):
    job = Job('ab_tfidf_elasticnet')
    pipeline = Pipeline(steps=[("tfidf", TfidfVectorizer(stop_words = 'english')),
                               ('elnet', SGDClassifier(penalty="elasticnet"))])
    parameters = dict(tfidf__norm = ['l2'],
                      tfidf__ngram_range = [(1, 3)],
                      elnet__alpha = [1e-4, 1e-3, 1e-2, 1e-1],
                      elnet__l1_ratio = [0.1, 0.5, 0.8, 0.9, 0.99])
    job.run(pipeline, parameters, data)
    return None

def ac_truncSVD_GBM(data):
    job = Job('ac_truncSVD_GBM')
    pipeline = Pipeline(steps=[("tfidf", TfidfVectorizer(stop_words = 'english')),
                               ("trunc_svd", TruncatedSVD()),
                               ('gbm', GradientBoostingClassifier())])
    parameters = dict(tfidf__norm = ['l2'],
                      tfidf__ngram_range = [(1, 2)],
                      trunc_svd__n_components = [900],
                      gbm__n_estimators = [270])
    job.run(pipeline, parameters, data)
    return None

def ad_truncSVD_randomForest(data):
    job = Job('ad_truncSVD_randomForest')
    pipeline = Pipeline(steps=[("tfidf", TfidfVectorizer(stop_words = 'english')),
                               ("trunc_svd", TruncatedSVD()),
                               ('rf', RandomForestClassifier())])
    parameters = dict(tfidf__norm = ['l2'],
                      tfidf__ngram_range = [(1, 2)],
                      trunc_svd__n_components = [900],
                      rf__n_estimators = [30, 90, 270])
    job.run(pipeline, parameters, data)
    return None

def ae_tfidf_svm(data):
    job = Job('ae_tfidf_svm')
    pipeline = Pipeline(steps=[("tfidf", TfidfVectorizer(stop_words = 'english')),
                               ('svm', SVC())])
    # set probability true in future runs
    parameters = dict(tfidf__norm = ['l2'],
                      tfidf__ngram_range = [(1, 2)],
                      svm__C = np.logspace(-1, 1, 3),
                      svm__kernel = ['linear'])
    job.run(pipeline, parameters, data)
    return None
        
def af_AdaBoost_Extratree(data):
    job = Job('af_AdaBoost_Extratree', ensamble_model_job = True)
    pipeline = Pipeline(steps=[("tfidf", TfidfVectorizer(stop_words = 'english')),
                               ("trunc_svd", TruncatedSVD()),
                               ('ada_extraTree', 
                                AdaBoostClassifier(ExtraTreesClassifier()))])
    parameters = dict(tfidf__norm = ['l2'],
                      tfidf__ngram_range = [(1, 2)],
                      trunc_svd__n_components = [900],
                      ada_extraTree__base_estimator__n_estimators = [100])
    job.run(pipeline, parameters, data)
    return None

def ag_tfidf_NaiveBayes(data):
    job = Job('ag_tfidf_NaiveBayes')
    pipeline = Pipeline(steps=[("tfidf", TfidfVectorizer(stop_words = 'english')),
                               ('nb', MultinomialNB())])
    parameters = dict(tfidf__norm = ['l2'],
                      tfidf__ngram_range = [(1, 2)],
                      nb__alpha = [0, 0.1, 1, 10 ])
    job.run(pipeline, parameters, data)
    return None    

def ah_vecAvg_multinomial(data):
    job = Job('ah_vecAvg_multinomial')
    pipeline = Pipeline(steps=[("vecAvg", Word2VecTransformer(fld.get_path(fld.model_meta_data, fl_word_vectors), 
                                                              dim = 300,
                                                              all_text_data = list(data.df.text))),
                               ('logistic', LogisticRegression())])
    parameters = dict(logistic__C = [1])
    job.run(pipeline, parameters, data)
    return None

def ai_vecAvg_randomForest(data):
    job = Job('ai_vecAvg_randomForest')
    pipeline = Pipeline(steps=[("vecAvg", Word2VecTransformer(fld.get_path(fld.model_meta_data, fl_word_vectors), 
                                                              dim = 300,
                                                              all_text_data = list(data.df.text))),
                               ('rf', RandomForestClassifier())])
    parameters = dict(rf__n_estimators = [30, 90, 270])
    job.run(pipeline, parameters, data)
    return None    

# Ensamble Pipelines: ---------------------------------------------------------
def ea_multinon(data):
    job = Job('ea_multinon', ensamble_model_job = True)
    pipeline = Pipeline(steps=[('logistic', LogisticRegression())])
    parameters = dict(logistic__C = np.logspace(-2, 2, 5))
    job.run(pipeline, parameters, data)
    return None

def eb_randomForest(data):
    job = Job('eb_randomForest', ensamble_model_job = True)
    pipeline = Pipeline(steps=[('rf', RandomForestClassifier())])
    parameters = dict(rf__n_estimators = [3, 9, 27])
    job.run(pipeline, parameters, data)
    return None
    
def ec_AdaBoost_Extratree(data):
    job = Job('ec_AdaBoost_Extratree', ensamble_model_job = True)
    pipeline = Pipeline(steps=[('ada_extraTree', 
                                AdaBoostClassifier(ExtraTreesClassifier()))])
    parameters = dict(ada_extraTree__base_estimator__n_estimators = [10])
    job.run(pipeline, parameters, data)
    return None
    
def ed_GBM(data):
    job = Job('ed_GBM', ensamble_model_job = True)
    pipeline = Pipeline(steps=[('gbm', GradientBoostingClassifier())])
    parameters = dict(gbm__n_estimators = [30, 100, 300])
    job.run(pipeline, parameters, data)
    return None
    

# Old Pipelines: --------------------------------------------------------------
def cnn(hidden_dims=100, max_features = 20000, embedding_dims = 128,
                 max_seq_len = 50, nb_filter = 50, filter_length = 5):
    model = Sequential()
    model.add(Embedding(max_features,
                        embedding_dims,
                        input_length=max_seq_len,
                        dropout=0.2))
    model.add(Convolution1D(nb_filter=nb_filter,
                            filter_length=filter_length,
                            border_mode='valid',
                            activation='relu',
                            subsample_length=1))
    def max_1d(X):
        return K.max(X, axis=1)

    model.add(Lambda(max_1d, output_shape=(nb_filter,)))
    model.add(Dense(hidden_dims))
    model.add(Dropout(0.5))
    model.add(Activation('relu'))
    model.add(Dense(1))
    model.add(Activation('sigmoid'))
    model.compile(loss='binary_crossentropy',
                  optimizer='adam',
                  metrics=['accuracy'])
    return model
        
def aj_embedding_cnn(data):
    job = Job('aj_embedding_cnn', cv = 4, n_threads = int(cpu_count() / 2))
    cnn_clf = KerasClassifier(build_fn=cnn, batch_size=32, nb_epoch=2)
    pipeline = Pipeline(steps=[('txt_to_seq', TextToNumericSequence()),
                               ('padd_seq', PadNumericSequence()),
                                ('cnn', cnn_clf)])
    parameters = dict(padd_seq__max_seq_len = [50],
                      cnn__max_seq_len = [50],
                      cnn__hidden_dims = [50])
    job.run(pipeline, parameters, data)
    return None

def lstm(max_features = 20000, embedding_dims = 128, max_seq_len = 100):
    model = Sequential()
    model.add(Embedding(max_features, embedding_dims, input_length=max_seq_len, 
                        dropout=0.2))
    model.add(LSTM(embedding_dims, dropout_W=0.2, dropout_U=0.2))  
    model.add(Dense(1))
    model.add(Activation('sigmoid'))
    model.compile(loss='binary_crossentropy',
                  optimizer='adam',
                  metrics=['accuracy'])        
    return model
        
def ak_embedding_lstm(data):
    job = Job('ak_embedding_lstm', cv = 4, n_threads = int(cpu_count() / 2))
    lstm_clf = KerasClassifier(build_fn=lstm, batch_size=32, nb_epoch=2)

    pipeline = Pipeline(steps=[('txt_to_seq', TextToNumericSequence()),
                               ('padd_seq', PadNumericSequence()),
                               ('lstm', lstm_clf)])
    parameters = dict(padd_seq__max_seq_len = [50],
                      lstm__max_seq_len = [50],
                      lstm__embedding_dims = [128],
                      lstm__nb_epoch = [3])
    job.run(pipeline, parameters, data)
    return None
    
    

def train_autoencoder_logistic(data):
    from keras.preprocessing.text import Tokenizer
    from keras.datasets import reuters
    
    tokenizer = Tokenizer(nb_words=20000)
    tokenizer.fit_on_texts(data.X_train().values)
    t = tokenizer.texts_to_matrix(data.X_train().values)
    
    X_train = tokenizer.sequences_to_matrix(data.X_train().values, mode='binary')
    print(t[0])
    print(len(t[0]))
    print(len(t))
    for i in t[0]:
        print(i)


    reuters.load_data(nb_words=max_words, test_split=0.2)
    
    
    input_size = data.X_train().shape[1]
    a = Input(shape=(input_size,))
    b = Dense(100)(a)
    c = Dense(32)(b)
    d = Dense(100)(c)    
    e = Dense(input_size)(d)
    model = Model(input=a, output=e)
    
    model.compile(loss='mean_squared_error', optimizer='rmsprop')
    model.fit(data.X_train().values, data.X_train().values,
              batch_size=64, nb_epoch=10, 
              validation_split = 0.1, callbacks = EarlyStopping())

    train = model.predict(data.X_train().values)
    test = model.predict(data.X_test().values)

    df_ind = np.concatenate([train, test])    
    df = np.concatenate([df_ind, data.df[data.fs_id].values, data.df[data.fs_target].values],
                   axis = 1)
   
    df = pd.DataFrame(df)
    df.columns = ['feature_autoencod' + str(i) for i in range(df_ind.shape[1])] + \
    data.fs_id +  data.fs_target
    
    
    data_trans = copy.deepcopy(data)
    data_trans.df = df
    data_trans.set_idx_train_valid_test(0.0)
    data_trans.set_fs(fs_target = data.fs_target,
                fs_id = data.fs_id,
                fs_ind = 'all')

    job = Job('ca_autoencoder_logistic')
    pipeline = Pipeline(steps=[('logistic', LogisticRegression())])
    parameters = dict(logistic__C = [1])
    job.run(pipeline, parameters, data_trans)
    return None
    
    
def main():
    data = Data(fl_sent_polarity_pos, fl_sent_polarity_neg)
    data.load_pre_processed(fl_sent_polarity_pre_proc)
    data.set_idx_train_valid_test(0)
    data.set_fs(fs_target = ['target'], fs_id = ['id'], fs_ind = 'text')
    
    data.describe()
        
    if False:
        # Metric: AUC || Holdout test data: yes
        data.set_idx_train_valid_test(valid_as_pct_of_train = 0.2)
        aa_tfidf_multinon(data)
        # score_LB: None   , score_train: 0.93247, score_cv: 0.81825, score_valid: 0.82127, param_best: {'tfidf__norm': 'l2', 'logistic__C': 1.0}
        ab_tfidf_elasticnet(data)
        # score_LB: None   , score_train: 0.8684 , score_cv: 0.78494, score_valid: 0.78742, param_best: {'elnet__alpha': 0.1, 'tfidf__norm': 'l2', 'elnet__l1_ratio': 0}
        ad_truncSVD_randomForest(data)
        # score_LB: None   , score_train: 1.0    , score_cv: 0.72456, score_valid: 0.72654, param_best: {'rf__n_estimators': 200, 'trunc_svd__n_components': 500, 'tfidf__norm': 'l1'}
        aa_tfidf_multinon(data) # accuracy
        # score_LB: None   , score_train: 0.8586 , score_cv: 0.74382, score_valid: 0.74262, param_best: {'logistic__C': 1.0, 'tfidf__norm': 'l2'}

        # Metric: Accuracy
        data.set_idx_train_valid_test(valid_as_pct_of_train = 0)
        aa_tfidf_multinon(data)
        # score_LB: None   , score_train: 0.86203, score_cv: 0.75249, score_valid: None   , param_best: {'tfidf__ngram_range': (1, 2), 'logistic__C': 1.0, 'tfidf__norm': 'l2'}, 
        ab_tfidf_elasticnet(data)
        # score_LB: None   , score_train: 0.93294, score_cv: 0.75446, score_valid: None   , param_best: {'tfidf__ngram_range': (1, 3), 'elnet__alpha': 0.0001, 'elnet__l1_ratio': 0.1, 'tfidf__norm': 'l2'}, time_model_training_min: 0.867
        ac_truncSVD_GBM(data)
        # score_LB: None   , score_train: 0.89177, score_cv: 0.69152, score_valid: None   , param_best: {'trunc_svd__n_components': 900, 'tfidf__ngram_range': (1, 2), 'gbm__n_estimators': 270, 'tfidf__norm': 'l2'}, time_model_training_min: 17.65
        ad_truncSVD_randomForest(data)
        # score_LB: None   , score_train: 0.99991, score_cv: 0.68233, score_valid: None   , param_best: {'trunc_svd__n_components': 900, 'tfidf__ngram_range': (1, 2), 'rf__n_estimators': 270, 'tfidf__norm': 'l2'}, time_model_training_min: 11.4
        ae_tfidf_svm(data)
        # score_LB: None   , score_train: 0.99212, score_cv: 0.76402, score_valid: None   , param_best: {'svm__C': 1.0, 'tfidf__norm': 'l2', 'tfidf__ngram_range': (1, 2), 'svm__kernel': 'linear'}, time_model_training_min: 2.7
        af_AdaBoost_Extratree(data)
        # score_LB: None   , score_train: 0.99991, score_cv: 0.5937 , score_valid: None   , param_best: {'ada_extraTree__base_estimator__n_estimators': 100, 'trunc_svd__n_components': 900, 'tfidf__norm': 'l2', 'tfidf__ngram_range': (1, 2)}, 
        ag_tfidf_NaiveBayes(data)
        # score_LB: None   , score_train: 0.99222, score_cv: 0.77012, score_valid: None   , param_best: {'tfidf__norm': 'l2', 'tfidf__ngram_range': (1, 2), 'nb__alpha': 1}, time_model_training_min: 0.1
        ah_vecAvg_multinomial(data)
        # score_LB: None   , score_train: 0.77912, score_cv: 0.75642, score_valid: None   , param_best: {'logistic__C': 1}, time_model_training_min: 1.167
        ai_vecAvg_randomForest(data)
        # score_LB: None   , score_train: 1.0    , score_cv: 0.71985, score_valid: None   , param_best: {'rf__n_estimators': 270}, time_model_training_min: 5.133
        aj_embedding_cnn(data)
        # score_LB: None   , score_train: 0.94391, score_cv: 0.76449, score_valid: None   , param_best: {'cnn__hidden_dims': 50}, time_model_training_min: 1.283
        ak_embedding_lstm(data)
        # score_LB: None   , score_train: 0.96305, score_cv: 0.75427, score_valid: None   , param_best: {'padd_seq__max_seq_len': 50, 'lstm__nb_epoch': 3, 'lstm__embedding_dims': 128, 'lstm__max_seq_len': 50}, time_model_training_min: 4.6
        
        data.create_ensamble_data()
        data.set_idx_train_valid_test(valid_as_pct_of_train = 0)
        data.set_fs(fs_target = ['target'], fs_id = ['id'], 
                    fs_ind = ['aa_tfidf_multinon', 'ab_tfidf_elasticnet',
                              'ae_tfidf_svm', 'ag_tfidf_NaiveBayes',
                              'ah_vecAvg_multinomial'])
        ea_multinon(data)
        # score_LB: None   , score_train: 0.99756, score_cv: 0.99728, score_valid: None   , param_best: {'logistic__C': 100.0}, time_model_training_min: 0.0
        eb_randomForest(data)
        # score_LB: None   , score_train: 0.99972, score_cv: 0.99766, score_valid: None   , param_best: {'rf__n_estimators': 9}, time_model_training_min: 0.017
        ec_AdaBoost_Extratree(data)
        # score_LB: None   , score_train: 1.0    , score_cv: 0.99747, score_valid: None   , param_best: {'ada_extraTree__base_estimator__n_estimators': 10}, 
        ed_GBM(data)
        # score_LB: None   , score_train: 1.0    , score_cv: 0.99756, score_valid: None   , param_best: {'gbm__n_estimators': 300}, time_model_training_min: 0.1
        
    else:
        data.set_idx_train_valid_test(valid_as_pct_of_train = 0)
        
    return None


if __name__ == "__main__":
    start_time = timeit.default_timer()
    main()
    print('Time taken (sec):', round(timeit.default_timer() - start_time))