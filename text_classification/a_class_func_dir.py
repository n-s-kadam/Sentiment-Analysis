# -*- coding: utf-8 -*-
"""
@author: nilesh

Tasks: Completed

Tasks: Pending
01. Log to file and console
03. Implement concept of run / batch 
07. Estimate time remaining for batch run
09. View progress of run
10. Convert ensamble function to class
20. Increase itteration automaticaly when ConvergenceWarning is desplayed
70. Fix elasticnet error (may be in predict funtion)
"""


from __future__ import print_function
import os
import sys
import pandas as pd
import simplejson
import numpy as np
import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning) 
import pickle
from multiprocessing import cpu_count
from datetime import datetime
import itertools


from configobj import ConfigObj
from sklearn.linear_model import LogisticRegression, ElasticNet
from sklearn.pipeline import Pipeline, FeatureUnion
from sklearn.decomposition import PCA
from sklearn.feature_selection import SelectKBest, SelectFromModel
from sklearn.neural_network import BernoulliRBM
from sklearn.preprocessing import PolynomialFeatures
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.multiclass import OutputCodeClassifier
from sklearn.decomposition import TruncatedSVD
from sklearn.feature_extraction import DictVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.grid_search import _CVScoreTuple
from sklearn.svm import SVC
from sklearn.cross_validation import train_test_split
from sklearn.grid_search import GridSearchCV
from sklearn.metrics import accuracy_score
from sklearn.base import BaseEstimator, TransformerMixin

from xgboost.sklearn import XGBClassifier
from keras.preprocessing.text import Tokenizer
from keras.preprocessing import sequence


# User Inputs -----------------------------------------------------------------
path_base = os.path.dirname(os.path.dirname(os.path.realpath(__file__))) 

# Set base path ---------------------------------------------------------------
os.chdir(path_base)
sys.path.append(path_base)

# Directory Structure ---------------------------------------------------------
class DirStructure():
    def __init__(self, fl_config = 'config.ini'):
        config = ConfigObj(fl_config)
        dir_structure = config['dir_structure']
        self.user_inputs = dir_structure ['fld_user_inputs']
        self.model_meta_data = dir_structure ['fld_model_meta_data']
        self.inp_data = dir_structure ['fld_inp_data']
        self.inp_data_pre_proc = dir_structure['fld_inp_data_pre_proc']
        self.data_featur_eng = dir_structure['fld_data_featur_eng']
        self.insights = dir_structure['fld_insights']
        self.model_pipeline = dir_structure['fld_model_pipeline']
        self.model_param_tun = dir_structure['fld_model_param_tun']
        self.model_scoring = dir_structure ['fld_model_scoring']
        self.model_result_summary = dir_structure['fld_model_result_summary']
        self.ensm_pipeline = dir_structure['fld_ensm_pipeline']
        self.ensm_param_tun = dir_structure['fld_ensm_param_tun']   
        self.ensm_scoring = dir_structure['fld_ensm_scoring']
        self.ensm_result_summary = dir_structure['fld_ensm_result_summary']
        self.model_verification = dir_structure ['fld_model_verification']
        self.unit_tests = dir_structure ['fld_unit_tests']
        self.scripts = dir_structure ['fld_scripts']
        self.docs = dir_structure ['fld_docs']
        self.logs = dir_structure ['fld_logs']

    def create_fld_if_not_exist(self, fld):
        if not os.path.exists(os.path.join(path_base, fld)):
            os.makedirs(os.path.join(path_base, fld))
            print('Created folder:', fld)

    def get_path(self, *paths):
        return os.path.join(path_base, *paths)
    
fld = DirStructure('config.ini')


# Classe: Models --------------------------------------------------------------


# Classes ---------------------------------------------------------------------
class Data:
    def __init__(self, fl_inp_train, fl_inp_test, fld_data = fld.inp_data, 
                 fl_pkl = 'a_data.pkl'):
        self.fl_inp_train = fl_inp_train
        self.fl_inp_test = fl_inp_test
        self.fld_data = fld_data
        self.fl_pkl = fl_pkl
        
        self.n_train = None
        self.n_test = None
        self.idx_train = None
        self.idx_valid = None
        self.idx_test = None
        self.valid_as_pct_of_train = None

        self.fs_target = []
        self.fs_id = []
        self.fs_ind = []
        self.fs_categorical = []
        self.fs_remove = []

    def run(self, options = ['create_save', 'pre_process', 'save', 'describe', 
                             'explore', 'load']):
        if 'create_save' in options:
            self.create_save_data()
        if 'pre_process' in options:
            self.pre_process(options = ['remove_constant_cols',
                                          'remove_duplicated_cols'])
        if 'describe' in options:
            self.describe()
        if 'explore' in options:
            self.explore()
        if 'load' in options:
            self.load()
    
    def create_save_data(self, fld = fld):
        train = pd.read_csv(fld.get_path(self.fld_data, self.fl_inp_train)) 
        test = pd.read_csv(fld.get_path(self.fld_data, self.fl_inp_test))    
        df = pd.concat([train, test], ignore_index = True)
        train['sent'] = train['sent'].apply(lambda l : [1 if l == 1 else 0])
        test['sent'] = test['sent'].apply(lambda l : [1 if l == 1 else 0])
        self.df = df
        self.n_train = train.shape[0]
        self.n_test = test.shape[0] 
        with open(fld.get_path(self.fld_data, self.fl_pkl), 'wb') as fl:
            pickle.dump(self.__dict__, fl, -1)
        
    def load(self, fld = fld):
        with open(fld.get_path(self.fld_data, self.fl_pkl), 'rb') as fl:
            tmp_dict = pickle.load(fl)    
        self.__dict__.update(tmp_dict)

    def load_pre_processed(self, fl, fld = fld):
        train = pd.read_csv(fld.get_path(fld.inp_data_pre_proc, fl))
        test = pd.read_csv(fld.get_path(fld.inp_data_pre_proc, fl))
        df = pd.concat([train, test], ignore_index = True)
        df['id'] = range(df.shape[0])
        df['target'] = df['target'].apply(lambda x: 1 if x == 'positive' else 0)
        self.df = df
        self.n_train = train.shape[0]
        self.n_test = test.shape[0] 
        return None
        
    def pre_process(self, options = ['inpute_na',
                                     'remove_constant_cols', 
                                     'remove_duplicated_cols', 
                                     'transform_log']):
        self.df.srch_ci_year = self.df.srch_ci.apply(lambda s: str(s)[0:3])
        self.df.drop('srch_ci', axis=1, inplace=True)
        self.df.srch_co_year = self.df.srch_co.apply(lambda s: str(s)[0:3])
        self.df.drop('srch_co', axis=1, inplace=True)

        self.df.drop('date_time', axis=1, inplace=True)
        print(self.fs_ind)
        print(type(self.fs_ind))
        self.fs_ind.remove('srch_ci')
        self.fs_ind.remove('srch_co')
        self.fs_ind.remove('date_time')
        
        if 'inpute_na' in options:
            for col in self.fs_ind:
                tt = self.df[col].values
                tt[np.isnan(tt)] = np.nanmean(tt)
                self.df[col] = tt
        
        if 'remove_constant_cols' in options:
            # remove constant columns (std = 0)
            for col in self.fs_ind:
                if self.df[col].std() == 0:
                    self.fs_remove.append(col)
        
        if 'remove_duplicated_cols' in options:
            cols = self.fs_ind
            for i in range(len(cols)-1):
                v = self.df[cols[i]].values
                for j in range(i+1,len(cols)):
                    if np.array_equal(v,self.df[cols[j]].values):
                        self.fs_remove.append(cols[j])  
        self.fs_remove = list(set(self.fs_remove))

        if 'transform_log' in options:
            # transformation: log
            for col in self.fs_ind:
                if np.min(self.df[col]) < 0:
                    self.df[col] = self.df[col].add(1 - np.min(self.df[col]))
                    # print(col,':', np.min(self.df[col]))
                    self.df[col] = np.log(self.df[col])
    
    def describe(self):
        print(self.key_veriables())
        print(self.df.head())
    
    def explore(self):
        pass
    
    def create_ensamble_data(self, fld = fld):
        train, test = self.df.iloc[self.idx_train], self.df.iloc[self.idx_test]
        train = train[self.fs_id + self.fs_target]
        test = test[self.fs_id + self.fs_target]
        for dirpath, dirnames, filenames in os.walk(fld.get_path(fld.model_scoring)):
            for filename in [f for f in filenames if f.endswith("_train.csv")]:
                train = pd.merge(train, pd.read_csv(os.path.join(dirpath, filename)),
                           how = 'left', on = self.fs_id, copy = False)
                train.columns.values[-1] = '_'.join(os.path.splitext(filename)[0].split('_')[:-1])
        for dirpath, dirnames, filenames in os.walk(fld.get_path(fld.model_scoring)):
            for filename in [f for f in filenames if f.endswith("_test.csv")]:
                test = pd.merge(test, pd.read_csv(os.path.join(dirpath, filename)),
                           how = 'left', on = self.fs_id, copy = False)
                test.columns.values[-1] = '_'.join(os.path.splitext(filename)[0].split('_')[:-1])
        
        train.columns.values[1] = str(self.fs_target[0])
        test.columns.values[1] = str(self.fs_target[0])
        train.to_csv(fld.get_path(fld.ensm_scoring, 'train.csv'), index=False)
        test.to_csv(fld.get_path(fld.ensm_scoring, 'test.csv'), index=False)
        
        # dat = train.append(test)
        dat = pd.concat([train, test], ignore_index=True)
        dat = dat.sort_values(self.fs_id)
        dat = dat.reset_index(drop = True)
        '''
        dat = pd.merge(self.df.reset_index(drop = True), 
                       dat.reset_index(drop = True),
                       how = 'left', on = self.fs_id[0], copy = False)
        if self.fs_target[0] + '_y' in dat.columns:
            dat = dat.drop(self.fs_target[0] + '_y', axis = 1)
        dat.rename(columns = {self.fs_target[0] + '_x' : self.fs_target[0]}, 
                                  inplace = True)
        '''
        self.df = dat
        return None
        
    def set_idx_train_valid_test(self, valid_as_pct_of_train = 0.2):
        self.valid_as_pct_of_train = valid_as_pct_of_train
        idx_train_valid = range(self.n_train)
        self.idx_test = [self.n_train + i for i in range(self.n_test)] 
        self.idx_train, self.idx_valid = \
            train_test_split(idx_train_valid, 
                             test_size=self.valid_as_pct_of_train,
                             random_state=111)
        return None
    
    def set_fs(self, fs_target, fs_id, fs_ind):
        self.fs_target = list(fs_target)
        self.fs_id = list(fs_id)
        if fs_ind == 'all':
            fs_ind = list(set(self.df.columns) - \
                     set(self.fs_id + self.fs_target + self.fs_remove))
        self.fs_ind = fs_ind
        return None

    def fs_categorical(self):
        # WIP: 
        # return list(set(self.fs_ind) - set(self.fs_categorical))
        pass
    
    def X_train(self):
        return self.df.iloc[self.idx_train][self.fs_ind]

    def X_valid(self):
        return self.df.iloc[self.idx_valid][self.fs_ind]

    def X_test(self):
        return self.df.iloc[self.idx_test][self.fs_ind]

    def y_train(self):
        return self.df.iloc[self.idx_train][self.fs_target]
        
    def y_valid(self):
        return self.df.iloc[self.idx_valid][self.fs_target]

    def y_test(self):
        return self.df.iloc[self.idx_test][self.fs_target]

    def key_veriables(self):
        return {'n_train' : self.n_train, 'n_test' : self.n_test, 
                              'valid_as_pct_of_train' : \
                              self.valid_as_pct_of_train, 
                              'fs_ind_count': len(self.fs_ind),
                              'fs_all_count': len(self.df.columns),
                              'fs_remove_count': len(self.fs_remove)}


    
    
class Result():
    def __init__(self):
        self.score_train = None
        self.score_valid = None
        self.scroe_cv = None
        self.score_leader_board = None
        self.scores_grid = None
        
    def print_veriables(self):
        print(self.__dict__)
        

class Job(Data):
    def __init__(self, job_name, ensamble_model_job = False,
                 fl_submission_input = None, cv = 10, scoring = 'accuracy', 
                 n_threads = cpu_count()-1):
        self.job_name = job_name
        self.job_name_full = ''
        self.fl_submission_created = self.job_name
        self.fl_submission_input = fl_submission_input
        self.cv = cv
        self.scoring = scoring
        self.n_threads = n_threads
        self.parameters_best = None
        self.time_start = None
        self.ensamble_model_job = ensamble_model_job
        
    def run(self, pipeline, parameters, data):
        result = Result()
        self.time_start = datetime.now()
        model = self.grid_search_and_fit_model(pipeline, parameters, data)
        self.time_model_training_min = (datetime.now() - 
                                        self.time_start).seconds / 60
        self.evaluate_model(data, model, result)
        self.print_and_save_result(data, model, result)
        self.write_submission(data, model, result)
        
    def grid_search_and_fit_model(self, pipeline, parameters, data):
        for step in pipeline.steps:
            if step[1].__class__.__name__ == 'ElasticNetCV':
                Warning('Not implemented ElasticNetCV')
        if self.cv == 0:
            model = pipeline.set_params(**parameters)
        else:
            model = GridSearchCV(pipeline, parameters, cv = self.cv, scoring = self.scoring,
                                 n_jobs = self.n_threads, verbose = 3)
        model.fit(data.X_train().values, 
                  data.y_train().values.ravel())
        return model
    
    def predict(self, model, X, predict_class = False):
        if predict_class:
            return model.predict(X)
        try:
            out = model.predict_proba(X)[:,1]
        except:
            out = model.predict(X)
        return out
        
    def evaluate_model(self, data, model, result):
        result.score_train = accuracy_score(data.y_train().values.ravel(),
                                 self.predict(model, data.X_train().values, 
                                              predict_class = True))
        if(len(data.idx_valid) > 0):
            result.score_valid = accuracy_score(data.y_valid().values.ravel(), 
                                               self.predict(model, data.X_valid().values, 
                                               predict_class = True))                    
        result.score_cv = model.best_score_
        temp = list()
        for score in model.grid_scores_:
            temp.append(_CVScoreTuple(
                score[0],
                score[1],
                list(score[2])))
                
        result.scores_grid = temp

        self.parameters_best = model.best_params_
        for step in model.estimator.steps:
            self.job_name_full = str(self.job_name_full) + '_' + step[0]
        return None
    
    def print_and_save_result(self, data, model, result, debug = False, 
                              save_result = True, round_digits = 5, fld = fld):
        self.time_start = self.time_start.isoformat()
        veriables_all = data.key_veriables()
        veriables_all.update(result.__dict__)
        veriables_all.update(self.__dict__)
        
        if debug:
            print(simplejson.dumps(veriables_all))

        def formated_str(x):
            if isinstance(x, np.float64):
                x = round(x, round_digits)
            return str(x).ljust(round_digits+2)
            
        result_summary = ('# score_LB: ', formated_str('None'), ', ',
                          'score_train: ', formated_str(result.score_train), ', ',
                          'score_cv: ', formated_str(result.score_cv), ', ',
                          'score_valid: ', formated_str(result.score_valid), ', ',
                          'param_best: ', str(self.parameters_best), ', ',
                          'time_model_training_min: ', 
                          str(round(self.time_model_training_min, 3)))
        print(''.join(result_summary))

        if self.ensamble_model_job:
            fld_save = fld.ensm_result_summary
        else:
            fld_save = fld.model_result_summary

        if save_result:
            with open(fld.get_path(fld_save, 
                                   self.job_name + '.json'), 'w') as f_out:
                simplejson.dump(simplejson.dumps(veriables_all), f_out)
            with open(fld.get_path(fld_save, 
                                   self.job_name + '.txt'), 'w') as f_out:
                f_out.write(''.join(result_summary))
        return None
    
    def write_submission(self, data, model, result, fld = fld):
        if self.ensamble_model_job:
            fld_save = fld.ensm_scoring
        else:
            fld_save = fld.model_scoring

        fld.create_fld_if_not_exist(fld.get_path(fld_save, self.job_name))
        if self.fl_submission_input is not None:
            sub = pd.read_csv(fld.get_path(fld.inp_data, self.fl_submission_input))
        else:
            sub = data.df.iloc[data.idx_test][data.fs_id + data.fs_target].reset_index(drop=True)
        output_test = self.predict(model, data.X_test().values)
        
        test = pd.concat([sub[data.fs_id].reset_index(drop = True), 
                          pd.DataFrame(output_test).reset_index(drop = True)], axis = 1,                           
                          ignore_index = True)           
        test.columns = [data.fs_id + data.fs_target]
        test.to_csv(fld.get_path(fld_save, self.job_name, 
                            self.fl_submission_created + '_test.csv'),
                   index = False)
        
        output_train = self.predict(model, data.X_train().values)
        train = pd.concat([data.df.iloc[data.idx_train][data.fs_id].reset_index(drop = True), 
                          pd.DataFrame(output_train).reset_index(drop = True)], axis = 1,                           
                          ignore_index = True)
        train.columns = [data.fs_id + data.fs_target]
        train = train.sort_values(data.fs_id)
        train.to_csv(fld.get_path(fld_save, self.job_name, 
                            self.fl_submission_created + '_train.csv'),
                   index = False)     
        return None


# Custom Preprocessors: -------------------------------------------------------
class Word2VecTransformer(BaseEstimator, TransformerMixin):
    def __init__(self, path_word_vectors, dim = 300, all_text_data = None):
        self.path_word_vectors = path_word_vectors
        self.dim = dim
        self.all_text_data = all_text_data
        
    def fit(self, x, y=None):
        return self

    def transform(self, text, y=None):
        nested_list = [line.split(' ') for line in text]
        word_list = set(list(itertools.chain(*nested_list)))
        word_vectors = self.load_word_vectors(word_list)
        
        features = []
        for line in text:
            feature = [0] * self.dim
            for w in line.split(' '):
                feature = np.add(feature, word_vectors.get(w, [0] * self.dim))
            features.append(feature)
        # print('length features:', len(features))
        # print('length feature:', len(features[0]))
        # print('feature:', features[0])
        return features

    def load_word_vectors(self, word_list):
        word_vectors = dict()
        with open(self.path_word_vectors, 'r') as f: 
            counter = 1
            if self.all_text_data is not None:
                for line in f:
                    splits = line.strip().split(' ')
                    if splits[0] in word_list:
                        word_vectors[splits[0]] = np.array(splits[1:], dtype=np.float)
                    counter += 1
            else:
                for line in f:
                    splits = line.strip().split(' ')
                    word_vectors[splits[0]] = np.array(splits[1:], dtype=np.float)
                    counter += 1
        return word_vectors        
        
class TextToNumericSequence(BaseEstimator, TransformerMixin):
    def __init__(self, n_max_features = 20000):
        self.n_max_features = n_max_features
        self.tokenizer = Tokenizer(nb_words=n_max_features)
    
    def fit(self, text, y=None):
        self.tokenizer.fit_on_texts(text)
        return self

    def transform(self, text, y=None):
        return self.tokenizer.texts_to_sequences(text)

class PadNumericSequence(BaseEstimator, TransformerMixin):
    def __init__(self, max_seq_len = 80):
        self.max_seq_len = None
        
    def fit(self, text, y=None):
        return self

    def transform(self, m, y=None):
        return sequence.pad_sequences(m, maxlen=self.max_seq_len)


# Standard Pipelines: ---------------------------------------------------------
def xx_kbest_logistic(data):
    job = Job('xx_kbest_logistic')
    pipeline = Pipeline(steps=[("univ_select", SelectKBest()),
                               ('logistic', LogisticRegression())])
    parameters = dict(univ_select__k=[15, 25, 35],
                      logistic__C = [1])
    job.run(pipeline, parameters, data)
    return None

def xx_outputCode_logistic(data):
    job = Job('xx_outputCode_logistic')    
    pipeline = Pipeline(steps=[('logistic', OutputCodeClassifier(LogisticRegression()))])
    parameters = dict(logistic__code_size = [0.2])    
    job.run(pipeline, parameters, data)
    return None

def xx_kbest_polinon_kbest_logistic(data):
    job = Job('xx_kbest_polinon_kbest_logistic')
    pipeline = Pipeline(steps=[("univ_select_1", SelectKBest()),
                               ('polinom', PolynomialFeatures()),
                               ("univ_select_2", SelectKBest()),
                               ('logistic', LogisticRegression())])
    parameters = dict(univ_select_1__k=[60],
                      polinom__degree = [2],
                      univ_select_2__k=[120, 240],
                      logistic__C = np.logspace(-1, 1, 3))
    job.run(pipeline, parameters, data)
    return None

def xx_pca_logistic(data):
    job = Job('xx_pca_logistic')
    pipeline = Pipeline(steps=[("pca", PCA()),
                               ('logistic', LogisticRegression())])
    parameters = dict(pca__n_components = [40, 'mle'],
                      logistic__C = np.logspace(-1, 1, 3))
    job.run(pipeline, parameters, data)
    return None
    
def xx_kbest_rbm_logistic(data):
    job = Job('xx_kbest_rbm_logistic')
    pipeline = Pipeline(steps=[("univ_select_1", SelectKBest()),
                               ('rbm', BernoulliRBM()),
                               ('logistic', LogisticRegression())])
    parameters = dict(univ_select_1__k=[40, 60],
                      rbm__n_components = [15, 30],
                      logistic__C = np.logspace(-1, 1, 3))
    job.run(pipeline, parameters, data)
    return None
    
def xx_kbest_polinom_kbest_pca_logistic(data):
    job = Job('xx_kbest_polinom_kbest_pca_logistic')
    pipeline = Pipeline(steps=[("univ_select_1", SelectKBest()),
                               ('polinom', PolynomialFeatures()),
                               ("univ_select_2", SelectKBest()),
                               ('pca', PCA()),
                               ('logistic', LogisticRegression())])
    parameters = dict(univ_select_1__k=[60],
                      polinom__degree = [2],
                      univ_select_2__k=[120],
                      pca__n_components = [40, 'mle'],
                      logistic__C = [0.1, 1])
    job.run(pipeline, parameters, data)
    return None


def xx_fsSelectFromModel_logistic(data):
    job = Job('xx_fsSelectFromModel_logistic')
    pipeline = Pipeline(steps=[('feature_selection', 
                                SelectFromModel(ExtraTreesClassifier())),
                               ('logistic', LogisticRegression())])
    parameters = dict(logistic__C = [0.1, 1, 10])
    job.run(pipeline, parameters, data)
    return None


def xx_kbest_polinom_kbest_randomForest(data):
    job = Job('xx_kbest_polinom_kbest_randomForest')
    pipeline = Pipeline(steps=[("univ_select_1", SelectKBest()),
                               ('polinom', PolynomialFeatures()),
                               ("univ_select_2", SelectKBest()),
                               ('rf', RandomForestClassifier())])
    parameters = dict(univ_select_1__k=[60],
                      polinom__degree = [2],
                      univ_select_2__k=[120],
                      rf__n_estimators = [50, 100, 500])
    job.run(pipeline, parameters, data)                  
    return None


def xx_fsUnion__kbest_polinom_pca_rbm__logistic(data):
    job = Job('xx_fsUnion__kbest_polinom_pca_rbm__logistic')    
    combined_features = FeatureUnion([("univ_select", SelectKBest()),
                                      ('polinom', PolynomialFeatures()),
                                      ("pca", PCA()),
                                      ("rbm", BernoulliRBM())])    
    pipeline = Pipeline(steps=[('features', combined_features),
                               ('logistic', LogisticRegression())])
    parameters = dict(features__univ_select__k=[60],
                      features__polinom__degree = [2],
                      features__pca__n_components=[15, 'mle'],
                      features__rbm__n_components = [15, 50],
                      logistic__C = [0.001, 0.01, 0.1])
    job.run(pipeline, parameters, data)
    return None

def xx_kbest_polinom_kbest_elasticNet(data):
    job = Job('xx_kbest_polinom_kbest_elasticNet')
    pipeline = Pipeline(steps=[("univ_select_1", SelectKBest()),
                               ('polinom', PolynomialFeatures()),
                               ("univ_select_2", SelectKBest()),
                               ('en', ElasticNet(max_iter = 3000))])
    parameters = dict(univ_select_1__k=[60],
                      polinom__degree = [2],
                      univ_select_2__k=[250, 1000],
                      en__l1_ratio = [0.1])
    job.run(pipeline, parameters, data)

def xx_XGBoost(data):
    job = Job('xx_XGBoost')
    pipeline = Pipeline(steps=[('xgb', XGBClassifier())])
    parameters = dict(xgb__n_estimators = [400, 560, 650],
                      xgb__objective = ["binary:logistic"], 
                      xgb__learning_rate = [0.01, 0.0202048],
                      xgb__max_depth = [5, 7, 10],
                      xgb__subsample = [0.6815, 0.8],
                      xgb__colsample_bytree = [0.701, 0.8])
    job.run(pipeline, parameters, data)
    return None
    
def xx_fsUnion__polinom_pca_rbm__XGBoost(data):
    job = Job('xx_fsUnion__polinom_pca_rbm__XGBoost')    
    combined_features = FeatureUnion([("univ_select", SelectKBest()),
                                      ('polinom', PolynomialFeatures()),
                                      ("pca", PCA()),
                                      ("rbm", BernoulliRBM())])    
    pipeline = Pipeline(steps=[('features', combined_features),
                               ('xgb', XGBClassifier())])
    parameters = dict(features__univ_select__k=['all'],
                      features__polinom__degree = [2],
                      features__pca__n_components=[18],
                      features__rbm__n_components = [15])
    job.run(pipeline, parameters, data)
    return None



def zz_feature_union(data):
    from sklearn.base import BaseEstimator, TransformerMixin
    from sklearn.datasets.twenty_newsgroups import strip_newsgroup_footer
    from sklearn.datasets.twenty_newsgroups import strip_newsgroup_quoting
    class ItemSelector(BaseEstimator, TransformerMixin):
        """For data grouped by feature, select subset of data at a provided key.
    
        The data is expected to be stored in a 2D data structure, where the first
        index is over features and the second is over samples.  i.e.
    
        >> len(data[key]) == n_samples
    
        Please note that this is the opposite convention to sklearn feature
        matrixes (where the first index corresponds to sample).
    
        ItemSelector only requires that the collection implement getitem
        (data[key]).  Examples include: a dict of lists, 2D numpy array, Pandas
        DataFrame, numpy record array, etc.
    
        >> data = {'a': [1, 5, 2, 5, 2, 8],
                   'b': [9, 4, 1, 4, 1, 3]}
        >> ds = ItemSelector(key='a')
        >> data['a'] == ds.transform(data)
    
        ItemSelector is not designed to handle data grouped by sample.  (e.g. a
        list of dicts).  If your data is structured this way, consider a
        transformer along the lines of `sklearn.feature_extraction.DictVectorizer`.
    
        Parameters
        ----------
        key : hashable, required
            The key corresponding to the desired value in a mappable.
        """
        def __init__(self, key):
            self.key = key
    
        def fit(self, x, y=None):
            return self
    
        def transform(self, data_dict):
            return data_dict[self.key]
    
    
    class TextStats(BaseEstimator, TransformerMixin):
        """Extract features from each document for DictVectorizer"""
    
        def fit(self, x, y=None):
            return self
    
        def transform(self, posts):
            return [{'length': len(text),
                     'num_sentences': text.count('.')}
                    for text in posts]
    
    
    class SubjectBodyExtractor(BaseEstimator, TransformerMixin):
        """Extract the subject & body from a usenet post in a single pass.
    
        Takes a sequence of strings and produces a dict of sequences.  Keys are
        `subject` and `body`.
        """
        def fit(self, x, y=None):
            return self
    
        def transform(self, posts):
            features = np.recarray(shape=(len(posts),),
                                   dtype=[('subject', object), ('body', object)])
            for i, text in enumerate(posts):
                headers, _, bod = text.partition('\n\n')
                bod = strip_newsgroup_footer(bod)
                bod = strip_newsgroup_quoting(bod)
                features['body'][i] = bod
    
                prefix = 'Subject:'
                sub = ''
                for line in headers.split('\n'):
                    if line.startswith(prefix):
                        sub = line[len(prefix):]
                        break
                features['subject'][i] = sub
    
            return features
    

    pipeline = Pipeline([
    # Extract the subject & body
    ('subjectbody', SubjectBodyExtractor()),

    # Use FeatureUnion to combine the features from subject and body
    ('union', FeatureUnion(
        transformer_list=[

            # Pipeline for pulling features from the post's subject line
            ('subject', Pipeline([
                ('selector', ItemSelector(key='subject')),
                ('tfidf', TfidfVectorizer(min_df=50)),
            ])),

            # Pipeline for standard bag-of-words model for body
            ('body_bow', Pipeline([
                ('selector', ItemSelector(key='body')),
                ('tfidf', TfidfVectorizer()),
                ('best', TruncatedSVD(n_components=50)),
            ])),

            # Pipeline for pulling ad hoc features from post's body
            ('body_stats', Pipeline([
                ('selector', ItemSelector(key='body')),
                ('stats', TextStats()),  # returns a list of dicts
                ('vect', DictVectorizer()),  # list of dicts -> feature matrix
            ])),

        ],

        # weight components in FeatureUnion
        transformer_weights={
            'subject': 0.8,
            'body_bow': 0.5,
            'body_stats': 1.0,
        },
    )),

    # Use a SVC classifier on the combined features
    ('svc', SVC(kernel='linear')),
    ])
    return pipeline


# Functions: ------------------------------------------------------------------



# Run Code: -------------------------------------------------------------------
def main():
    pass

if __name__ == "__main__":
    main()