# -*- coding: utf-8 -*-
"""
@author: nilesh

Tasks: Completed
Tasks: Pending
"""

from __future__ import print_function
import os
import sys
import timeit
import pandas as pd
import re

# User Inputs: ----------------------------------------------------------------
path_base = os.path.dirname(os.path.dirname(os.path.realpath(__file__))) 
fl_sent_polarity_pos = 'rt-polaritydata/rt-polarity.pos'
fl_sent_polarity_neg = 'rt-polaritydata/rt-polarity.neg'
fl_sent_polarity_pre_proc = 'sentiment_polarity_pre_proc.csv'


# Set Base Path: --------------------------------------------------------------
path_base = os.path.dirname(os.path.dirname(os.path.realpath(__file__))) 
os.chdir(path_base)
sys.path.append(path_base)
from q_scripts.a_class_func_dir import DirStructure


# Read Config File: -----------------------------------------------------------
fld = DirStructure('config.ini')

# Preprocess Functions: -------------------------------------------------------
def clean_str(string):
    """
    Tokenization/string cleaning for all datasets except for SST.
    Original taken from https://github.com/yoonkim/CNN_sentence/blob/master/process_data.py
    """
    string = re.sub(r"[^A-Za-z0-9(),!?\'\`]", " ", string)
    string = re.sub(r"\'s", " \'s", string)
    string = re.sub(r"\'ve", " \'ve", string)
    string = re.sub(r"n\'t", " n\'t", string)
    string = re.sub(r"\'re", " \'re", string)
    string = re.sub(r"\'d", " \'d", string)
    string = re.sub(r"\'ll", " \'ll", string)
    string = re.sub(r",", " , ", string)
    string = re.sub(r"!", " ! ", string)
    string = re.sub(r"\(", " \( ", string)
    string = re.sub(r"\)", " \) ", string)
    string = re.sub(r"\?", " \? ", string)
    string = re.sub(r"\s{2,}", " ", string)
    return string.strip().lower()

def pre_proc_save_polarity_dataset(fl_sent_polarity_pos, fl_sent_polarity_neg,
                                   fl_sent_polarity_pre_proc):
    fl_path_pos = fld.get_path(fld.inp_data, fl_sent_polarity_pos)
    fl_path_neg = fld.get_path(fld.inp_data, fl_sent_polarity_neg)
    positive_examples = list(open(fl_path_pos, "r", encoding='utf-8', errors='ignore').readlines())
    positive_examples = [s.strip() for s in positive_examples]
    negative_examples = list(open(fl_path_neg, "r", encoding='utf-8', errors='ignore').readlines())
    negative_examples = [s.strip() for s in negative_examples]
    x_text = positive_examples + negative_examples
    x_text = [clean_str(sent) for sent in x_text]

    y = ['positive' for _ in positive_examples]
    y.extend(['negative' for _ in negative_examples])
    
    dat = pd.DataFrame({'id': range(len(y)), 'target': y, 'text': x_text})
    dat.to_csv(fld.get_path(fld.inp_data_pre_proc, fl_sent_polarity_pre_proc),
               index = False)
    return 

def load_preproc_polarity_dataset(fl_sent_polarity_pre_proc):
    return pd.read_csv(fld.get_path(fld.inp_data_pre_proc, fl_sent_polarity_pre_proc))


# Run Code: -------------------------------------------------------------------
def main():    
    pre_proc_save_polarity_dataset(fl_sent_polarity_pos, fl_sent_polarity_neg,
                                   fl_sent_polarity_pre_proc)
    
if __name__ == "__main__":
    start_time = timeit.default_timer()
    main()
    print('Time taken:', timeit.default_timer() - start_time)    
