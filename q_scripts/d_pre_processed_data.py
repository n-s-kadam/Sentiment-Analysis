# -*- coding: utf-8 -*-
"""
Created on Fri Oct 16 01:07:49 2015

@author: nilesh
"""

from __future__ import print_function
import os
import sys
import getpass

import numpy as np
import pandas as pd

import xgboost as xgb
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import OneHotEncoder
from sklearn.cross_validation import StratifiedKFold
from sklearn.metrics import log_loss
from sklearn.metrics import roc_auc_score
from sklearn.externals import joblib


# User Inputs -----------------------------------------------------------------
path_base = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))

fl_train_emoticon = 'a_emoticon.csv'
rows_to_read_from_file = None # 1000
fs_target = 'sentiment'
fs_id = 'id'

# Set Base Path----------------------------------------------------------------
os.chdir(path_base)
sys.path.append(path_base)
from q_scripts.a_load_function_dirs import *


# Functions: General ----------------------------------------------------------
def preprocessed_data(fl):
    dat = pd.read_csv(get_path(path_base, fld_inp_data, fl), 
                        nrows = rows_to_read_from_file, header = None)    
    dat.columns = ['sentiment', 'id', 'datetime', 'status', 'user', 'text']

    del dat['datetime']    
    del dat['status']    
    del dat['user']    

    fs_ind = list(set(dat.columns) - set([fs_target, fs_id]))
    fs_numeric = dat.select_dtypes(include=[np.number]).columns
    fs_categoric = list(set(fs_ind) - set(fs_numeric))
    
    return dat, fs_numeric, fs_categoric
    
def main():
    dat = preprocessed_data(fl_train_emoticon)
    
if __name__ == '__main__':
    main()
                