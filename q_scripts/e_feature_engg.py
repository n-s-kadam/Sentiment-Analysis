# -*- coding: utf-8 -*-
"""
Created on Fri Oct 16 01:07:49 2015

@author: nilesh
"""

from __future__ import print_function
import os
import sys

import numpy as np
import pandas as pd
import timeit



# User Inputs -----------------------------------------------------------------
path_base = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))

fl_w2v_goog_news = 'GoogleNews-vectors-negative300.bin'
fl_train_emoticon = 'a_emoticon.csv'
rows_to_read_from_file = None # 1000
fs_target = 'sentiment'
fs_id = 'id'

# Set Base Path----------------------------------------------------------------
os.chdir(path_base)
sys.path.append(path_base)
from q_scripts.a_load_function_dirs import *
from q_scripts.d_pre_processed_data import preprocessed_data


# Functions: General ----------------------------------------------------------

def load_bin_vec(fname):
    word_vecs = {}
    with open(fname, "rb") as f:
        header = f.readline()
        vocab_size, layer1_size = map(int, header.split())
        binary_len = np.dtype('float32').itemsize * layer1_size
        for line in xrange(vocab_size):
            word = []
            while True:
                ch = f.read(1)
                if ch == ' ':
                    word = ''.join(word)
                    break
                if ch != '\n':
                    word.append(ch)   
            word_vecs[word] = np.fromstring(f.read(binary_len), dtype='float32')  
    return word_vecs

def vector_from_line(line, w2v, k = 300):
    vec = np.zeros(k)
    for word in line.split():
        try:
            vec = np.add(vec , w2v[word])
        except KeyError:
            pass
    return vec
            
def feature_engineering(dat, fl):
    w2v = load_bin_vec(get_path(path_base, fld_model_meta_data, fl))

    dat['vec'] = dat.text.apply(lambda line: vector_from_line(line, w2v))    
    return dat
    
def main():
    start = timeit.default_timer()
    dat, fs_numeric, fs_categoric = preprocessed_data(fl_train_emoticon) 
    dat = feature_engineering(dat, fl_w2v_goog_news)
    stop = timeit.default_timer()
    print(stop - start)
    print(dat.head())
    print(dat.shape)
    
    
if __name__ == '__main__':
    main()
                