# -*- coding: utf-8 -*-
"""
Created on Fri Oct 16 01:07:49 2015

@author: nilesh
"""

from __future__ import print_function
import os
import sys
import getpass

import numpy as np
import pandas as pd

# import xgboost as xgb
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import OneHotEncoder
from sklearn.cross_validation import StratifiedKFold
from sklearn.metrics import log_loss
from sklearn.metrics import roc_auc_score
from sklearn.externals import joblib
from sklearn.cross_validation import train_test_split
from sklearn.grid_search import GridSearchCV
from sklearn.feature_extraction.text import HashingVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.linear_model import SGDClassifier, ElasticNetCV, LogisticRegression
from sklearn.grid_search import GridSearchCV
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import Imputer
from sklearn import linear_model, decomposition, datasets
from sklearn.pipeline import Pipeline
from multiprocessing import cpu_count


# User Inputs -----------------------------------------------------------------
path_base = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))

fl_train = 'train.csv'
fl_test = 'test.csv'
fl_sample_sub = 'sample_submission.csv'
fld_pre_processed = 'a_data_pre_processed'
fld_pre_processed = 'a_data_pre_processed'
fld_md_sc_tf_elas = 'a_model_tfidf_elas'
rows_to_read_from_file = None # 1000
fs_target = 'target'
fs_id = 'ID'

# Set Base Path----------------------------------------------------------------
os.chdir(path_base)
sys.path.append(path_base)
from q_scripts.a_load_function_dirs import *
from q_scripts.d_pre_processed_data import preprocessed_data
from q_scripts.e_feature_engg import feature_engineering

create_fld_if_not_exist(get_path(path_base, fld_model_scoring, fld_md_sc_tf_elas))

# Functions: Train Model ----------------------------------------------------------
def train_model(dat, fs_ind, fs_numeric, fs_categoric):
    idx_train, idx_test = train_test_split(range(dat.shape[0]), test_size = 0.3, 
                                           random_state = 123)
    
    
    parameters = {
        #'vect__max_df': [1.0],
        #'vect__max_features': (None, 5000, 10000, 50000),
        'vect__ngram_range': [(1, 1)],  # unigrams or bigrams
        #'tfidf__use_idf': (True, False),
        #'tfidf__norm': ('l1', 'l2'),
        # 'clf__alpha': [0.00001],
        # 'clf__loss': ['log'],
        #'clf__penalty': ['elasticnet'],
        #'clf__n_iter': (10, 50, 80),
    }

    pipeline = Pipeline([
        ('vect', HashingVectorizer()),
        #('tfidf', TfidfTransformer()),
        ('clf', ElasticNetCV()),
    ])
    
    grid_search = GridSearchCV(pipeline, param_grid=parameters, cv=3, 
                               n_jobs = cpu_count()-1, 
                               verbose = 3)
    
    model = grid_search.fit(dat.ix[idx_train, fs_ind].values.ravel(), 
                            dat.ix[idx_train, fs_target].ravel())
    print('Best estimator:', model.best_estimator_)
    print('Best parameters:', model.best_params_)
    print('Best score:', model.best_score_)
    
    out_test = model.predict(dat.ix[idx_test, fs_ind].values.ravel())
    print('AUC score:', roc_auc_score(dat.ix[idx_test, fs_target], out_test))
    
def train_model_num_log(dat, fs_ind, fs_numeric, fs_categoric):
    idx_train, idx_test = train_test_split(range(dat.shape[0]), test_size = 0, 
                                           random_state = 123)
    
    pipeline = Pipeline([
        ('imp', Imputer(missing_values='NaN', strategy='mean', axis=0)),
        ('clf', LogisticRegression()),
    ])
    
    model = pipeline.fit(dat.ix[idx_train, fs_ind].values, 
                            dat.ix[idx_train, fs_target].ravel())
    
    out_train = model.predict(dat.ix[idx_train, fs_ind].values)
    print('AUC score:', roc_auc_score(dat.ix[idx_train, fs_target], out_train))
    return model
    
def main():
    dat, fs_numeric, fs_categoric = preprocessed_data(fl_train)
    dat = feature_engineering(dat, fs_numeric, fs_categoric)
    '''    
    fs_ind = ['text']        
    model = train_model(dat, fs_ind, fs_numeric, fs_categoric)
    joblib.dump(model, get_path(path_base, fld_model_scoring, 
                                fld_md_sc_tf_elas, fld_md_sc_tf_elas))
    '''
    fs_ind= fs_numeric
    model = train_model_num_log(dat, fs_ind, fs_numeric, fs_categoric)
    fld_md_save = 'c_md_num_log'
    create_fld_if_not_exist(get_path(path_base, fld_model_scoring, fld_md_save))
    joblib.dump(model, get_path(path_base, fld_model_scoring, 
                                fld_md_save, fld_md_save))
    
    fld_md_save = 'c_md_num_log'
    model = joblib.load(get_path(path_base, fld_model_scoring, 
                                fld_md_save, fld_md_save))
    dat, fs_numeric, fs_categoric = preprocessed_data(fl_test)
    dat = feature_engineering(dat, fs_numeric, fs_categoric)
    fs_ind = fs_numeric
    idx_test = range(dat.shape[0])
    out_test = model.predict(dat.ix[idx_test, fs_ind].values)    
    out = pd.DataFrame(ID = dat.ix[idx_test, fs_id] ,target = out_test) 
    out.to_csv(get_path(path_base, fs_model_scoring, 'c_md_num_lr_scored.csv'))
    
if __name__ == '__main__':
    main()
                