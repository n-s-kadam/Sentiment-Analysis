# -*- coding: utf-8 -*-
"""
Created on Fri Oct  2 08:46:03 2015

@author: nilesh
"""

import os
import getpass
import sys

path_base = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))



# Directory Structure ---------------------------------------------------------
fld_user_inputs = 'a_user_inputs'
fld_model_meta_data = 'b_model_meta_data'
fld_inp_data = 'c_input_data'
fld_inp_data_pre_proc = 'd_pre_processed_data'
fld_data_featur_eng = 'e_feature_eng'
fld_model_pipeline = 'f_model_pipeline'
fld_model_param_tun = 'g_model_param_tun'
fld_model_scoring = 'h_model_scoring'
fld_model_result_sumr = 'i_model_result_summary'
fld_ensm_pipeline = 'j_ensm_pipeline'
fld_ensm_param_tun = 'k_ensm_param_tun'
fld_ensm_scoring = 'l_ensm_scoring'
fld_ensm_result_summary = 'm_ensm_result_summary'
fld_model_verification = 'n_model_verification'
fld_unit_tests = 'o_unit_tests'
fld_logs = 'p_logs'
fld_scripts = 'q_scripts'
fld_docs = 'r_documentation'



# Set base path ---------------------------------------------------------------
os.chdir(path_base)
sys.path.append(path_base)

# Classes ---------------------------------------------------------------------
class Data:
    def __init__(self, dat, idx_train, idx_test):
        self.dat = dat
        self.idx_train = idx_train
        self.idx_test = idx_test
    
    def get_dat_idx_train_idx_test():
        return self.dat, self.idx_train, self.idx_test


# Functions -------------------------------------------------------------------
def get_path(*paths):
    return os.path.join(*paths)
    
def create_fld_if_not_exist(fld):
    if not os.path.exists(os.path.join(path_base, fld)):
        os.makedirs(os.path.join(path_base, fld))
        print 'Created folder:', fld

def main():
    pass

if __name__ == "__main__":
    main()