# -*- coding: utf-8 -*-
"""
Created on Fri Oct  2 08:46:03 2015

@author: nilesh
"""
from __future__ import print_function
import os

path_base = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))

# Directory Structure ---------------------------------------------------------
fld_user_inputs = 'a_user_inputs'
fld_model_meta_data = 'b_model_meta_data'
fld_inp_data = 'c_input_data'
fld_inp_data_pre_proc = 'd_pre_processed_data'
fld_data_featur_eng = 'e_feature_eng'
fld_model_pipeline = 'f_model_pipeline'
fld_model_param_tun = 'g_model_param_tun'
fld_model_scoring = 'h_model_scoring'
fld_model_result_sumr = 'i_model_result_summary'
fld_ensm_pipeline = 'j_ensm_pipeline'
fld_ensm_param_tun = 'k_ensm_param_tun'
fld_ensm_scoring = 'l_ensm_scoring'
fld_ensm_result_summary = 'm_ensm_result_summary'
fld_model_verification = 'n_model_verification'
fld_unit_tests = 'o_unit_tests'
fld_logs = 'p_logs'
fld_scripts = 'q_scripts'
fld_docs = 'r_documentation'



# Set base path ---------------------------------------------------------------
os.chdir(path_base)

def print_fld_ver_names():
    for ver in globals():
        if ver[:4] == 'fld_':        
            print(ver)

def create_fld_if_not_exist(fld):
    if not os.path.exists(os.path.join(path_base, fld)):
        os.makedirs(os.path.join(path_base, fld))
        print('Created folder:', fld)

for fld in [fld_scripts,
fld_data_featur_eng,
fld_inp_data,
fld_model_pipeline,
fld_model_verification,
fld_logs,
fld_unit_tests,
fld_model_param_tun,
fld_ensm_result_summary,
fld_ensm_pipeline,
fld_user_inputs,
fld_model_scoring,
fld_model_meta_data,
fld_ensm_param_tun,
fld_docs,
fld_inp_data_pre_proc,
fld_model_result_sumr,
fld_ensm_scoring]:
    create_fld_if_not_exist(fld)
        